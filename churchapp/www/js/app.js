// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })
  .state('app.register', {
           url: "/register",
           views: {
               'menuContent': {
                   templateUrl: "templates/register.html",
                   controller: 'RegisterCtrl'
               }
           }
       })
.state('app.announcement', {
           url: "/announcement",
           views: {
               'menuContent': {
                   templateUrl: "templates/announcement.html",
                   controller: 'AnnouncementCtrl'
               }
           }
       })
  .state('app.search', {
    url: "/search",
    views: {
      'menuContent': {
        templateUrl: "templates/search.html"
      }
    }
  })
       .state('app.media', {
           url: "/media",
           views: {
               'menuContent': {
                   templateUrl: "templates/medias.html"
               }
           }
       })
       .state('app.mediasingle', {
           url: "/media/:mediaId",
           views: {
               'menuContent': {
                   templateUrl: "templates/media.html",
                   controller: 'MediasCtrl'
               }
           }
       })
       .state('app.friends', {
           url: "/friends",
           views: {
               'menuContent': {
                   templateUrl: "templates/friends.html",
                   controller: 'FriendsCtrl'
               }
           }
       })
	   

  .state('app.browse', {
    url: "/browse",
    views: {
      'menuContent': {
        templateUrl: "templates/browse.html"
      }
    }
  })
    .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent': {
          templateUrl: "templates/playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
    })
      //chat declaration
      .state('app.chat', {
          url: "/chat",
          views: {
              'menuContent': {
                  templateUrl: "templates/chats.html",
                  controller: 'ChatCtrl'
              }
          }
      })
	  .state('app.chatsingle', {
          url: "/chat/:chatId",
          views: {
              'menuContent': {
                  templateUrl: "templates/chat.html",
                 
              }
          }
      })
       //chat declaration
      .state('app.message', {
          url: "/message",
          views: {
              'menuContent': {
                  templateUrl: "templates/messages.html",
                  controller: 'MessagesCtrl'
              }
          }
      })
       //chat declaration
      .state('app.notification', {
          url: "/notification",
          views: {
              'menuContent': {
                  templateUrl: "templates/notification.html",
                  controller: 'NotificationCtrl'
              }
          }
      })
        //chat declaration
      .state('app.testimony', {
          url: "/testimony",
          views: {
              'menuContent': {
                  templateUrl: "templates/testimony.html",
                  controller: 'TestimonyCtrl'
              }
          }
      })

  .state('app.single', {
    url: "/playlists/:playlistId",
    views: {
      'menuContent': {
        templateUrl: "templates/playlist.html",
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/register');
});
