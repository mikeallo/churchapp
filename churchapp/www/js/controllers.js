angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})
    //chat controller
  .controller('ChatCtrl', function ($scope) {
      $scope.item = [];

  })
    //messages controller
     .controller('MessagesCtrl', function ($scope) {
         $scope.item = [];

     })
    // Notifications controller
     .controller('NotificationCtrl', function ($scope) {
          $scope.playlists = [
    { title: 'Every tuesday is digging deep by 6:30pm', id: 1 },
    { title: 'On wednesday is shilow hour by 10am in the morning', id: 2 },
    { title: 'On Thursdays, is our faith clinic by 6:30pm', id: 3 },
    { title: 'On Friday night is our vigil. Please, endeavor to come.', id: 4 },
    { title: 'On Sundays is our Service. The service starts by 8:30am. God bless you as you come.', id: 5 }
  ];

     })
	  .controller('RegisterCtrl', function ($scope) {
         $scope.item = [];

     })
     .controller('TestimonyCtrl', function ($scope) {
         $scope.item = [];

     })
     .controller('MediaCtrl', function ($scope) {
         $scope.item = [];

     })
    .controller('MediasCtrl', function($scope, $stateParams) {
    })
   .controller('FriendsCtrl', function ($scope) {
    $scope.item = [];
   })

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
